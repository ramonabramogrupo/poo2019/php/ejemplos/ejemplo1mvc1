<?php

namespace clases;


class Dado {
    public $numero;
    
    public function __construct() {
        $this->tirada();
    }
    
    public function tirada(){
        $this->numero= random_int(1, 6);
    }
    
    public function getNumero() {
        return $this->numero;
    }

    public function setNumero($numero) {
        $this->numero = $numero;
        return $this;
    }
    
    public function render(){
        $salida='<div class="dado">';
        $salida.=$this->getNumero();
        $salida.='</div>';
        return $salida;
    }
    
    public function __toString() {
        return $this->render();
    }


}
