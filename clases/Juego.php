<?php

namespace clases;

class Juego {
    public $dados=[];
    public function __construct($numeroDados) {
        for($c=0;$c<$numeroDados;$c++){
            $this->dados[]=new Dado();
        }
    }
    
    public function tirada(){
        foreach ($this->dados as $v) {
            $v->tirada();
        }
    }
    
    public function __toString() {
        $acumulador='<div class="tirada">';
        
        foreach ($this->dados as $v) {
            $acumulador.=$v;
        }
        $acumulador.='<br class="limpia">';
        $acumulador.="</div>";
        return $acumulador;
    }
}
