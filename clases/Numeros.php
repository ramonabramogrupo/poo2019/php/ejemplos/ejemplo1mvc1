<?php

namespace clases;

class Numeros {
    public $numeros=[];
    public $resultados=[];
    
    public function __construct($valores=[0,0]) {
        $this->numeros=$valores;
        $this->sumar();
        $this->restar();
        $this->producto();
        $this->cociente();
        $this->resto();
        
    }
    
    public function __toString() {
        return join(",",$this->numeros);
    }
    
    public function sumar(){
        $this->suma=$this->numeros[0]+$this-> numeros[1];
    }
    
    
    
    public function restar(){
        $this->resta=$this->numeros[0]-$this-> numeros[1];
    }
       
    
    public function producto(){
        $this->producto=$this->numeros[0]*$this-> numeros[1];
    }
           
    public function cociente(){
         if($this->numeros[1]==0){
             $this->cociente="-";
         }else{
             $this->cociente=$this->numeros[0]/$this-> numeros[1];
         }
        }
    
        
    public function resto(){
         if($this->numeros[1]==0){
             $this->resto="-";
         }else{
             $this->resto=$this->numeros[0]%$this-> numeros[1];
         }
        }
    
        
    public function __get($name) {
        return $this->resultados[$name];
    }
    
    public function __set($nombre, $valor) {
        $this->resultados[$nombre]=$valor;
    }
    
    public function __call($n, $a) {
        return "$n";
    }
}
