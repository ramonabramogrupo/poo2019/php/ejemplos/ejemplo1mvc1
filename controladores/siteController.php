<?php

namespace controladores;

class siteController extends Controller {

    private $miPie;
    private $miMenu;

    public function __construct() {
        parent::__construct();
        $this->miPie = "Ejemplo de Clase";
        $this->miMenu = [
            "Inicio" => $this->crearRuta(["accion" => "index"]),
            "Ejercicio 1" => $this->crearRuta(["accion" => "ejercicio1"]),
            "Ejercicio 2" => $this->crearRuta(["accion" => "ejercicio2"]),
            "Ejercicio 3" => $this->crearRuta(["accion" => "ejercicio3"])
        ];
    }

    public function indexAccion() {
        $this->render([
            "vista" => "index",
            "pie" => $this->miPie,
            "menu" => (new \clases\Menu($this->miMenu, "Inicio"))->html(),
            "menu1" => (new \clases\Menu([
                        "Ejercicio 1" => $this->crearRuta(["accion" => "ejercicio1"]),
                        "Ejercicio 2" => $this->crearRuta(["accion" => "ejercicio2"]),
                        "Ejercicio 3" => $this->crearRuta(["accion" => "ejercicio3"])
                            ], "Inicio"))
                    ->setClass_a("uno")
                    ->setClass_li("")
                    ->setClass_ul("")
                    ->setClass_nav("")
                    ->html(),
        ]);
    }

    public function ejercicio1Accion($objeto) {
        $pie = $this->miPie;
        if (empty($objeto->getValores())) {
            $objeto = new \clases\Numeros();
        } else {
            // entro si llegan datos
            $objeto = new \clases\Numeros($objeto->getValores()['numero']);
        }
        $this->render([
            "vista" => "ejercicio1",
            "pie" => $pie,
            "objeto" => $objeto,
            "menu" => (new \clases\Menu($this->miMenu, "Ejercicio 1"))->html()
        ]);
    }

    public function ejercicio2Accion($objeto) {
       $pie = $this->miPie;
        if (empty($objeto->getValores())) {
            $objeto = new \clases\Numeros();
        } else {
            // entro si llegan datos
            $objeto = new \clases\Numeros($objeto->getValores()['numero']);
        }
        $this->render([
            "vista" => "ejercicio2",
            "pie" => $pie,
            "objeto" => $objeto,
            "menu" => (new \clases\Menu($this->miMenu, "Ejercicio 2"))->html()
        ]);
    }
    
    public function ejercicio3Accion($objeto){
        $juego=new \clases\Juego(5);
        
        $this->render([
            "vista" => "ejercicio3",
            "pie" => $this->miPie,
            "juego" => $juego,
            "menu" => (new \clases\Menu($this->miMenu, "Ejercicio 3"))->html()
        ]);
    }

}
