<form method="get" action="<?= $this->crearRuta(["accion"=>"ejercicio1"])?>">
    <div>
    <label for="n1">Introduce numero 1</label>
    <input type="number" name="numero[]" id="n1" value="<?= $this->objeto->numeros[0] ?>">
    </div>
    <div>
    <label for="n2">Introduce numero 2</label>
    <input type="number" name="numero[]" id="n2" value="<?= $this->objeto->numeros[1] ?>">
    </div>
    <div>
    <label>Suma:</label>
    <input type="number" name="resultado" disabled value="<?= $this->objeto->getSuma() ?>">
    </div>
    
    <div>
        <button>Sumar</button>
    </div>
</form>

