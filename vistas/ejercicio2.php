<form method="get" action="<?= $this->crearRuta(["accion"=>"ejercicio2"])?>">
    <div>
    <label for="n1">Introduce numero 1</label>
    <input type="number" name="numero[]" id="n1" value="<?= $this->objeto->numeros[0] ?>">
    </div>
    <div>
    <label for="n2">Introduce numero 2</label>
    <input type="number" name="numero[]" id="n2" value="<?= $this->objeto->numeros[1] ?>">
    </div>
    <div>
    <label>Suma:</label>
    <input type="number" name="resultado[]" disabled value="<?= $this->objeto->suma ?>">
    </div>
    <label>Resta:</label>
    <input type="number" name="resultado[]" disabled value="<?= $this->objeto->resta ?>">
    </div>
    <label>Producto:</label>
    <input type="number" name="resultado[]" disabled value="<?= $this->objeto->producto ?>">
    </div>
    <label>Cociente:</label>
    <input type="text" name="resultado[]" disabled value="<?= $this->objeto->cociente ?>">
    </div>
    <label>Resto:</label>
    <input type="text" name="resultado[]" disabled value="<?= $this->objeto->resto ?>">
    </div>
    
    <div>
        <button>Sumar</button>
    </div>
</form>

